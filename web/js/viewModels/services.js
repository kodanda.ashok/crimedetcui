/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery',  'moment', 'ojs/ojbootstrap',
'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
'ojs/ojarraydataprovider', 'ojs/ojconverterutils-i18n',
'ojs/ojconverter-datetime', 'ojs/ojthemeutils',
'ojs/ojknockout', 'ojs/ojbutton',
'ojs/ojinputtext', 'ojs/ojinputnumber',
'ojs/ojradioset', 'ojs/ojcheckboxset', 'ojs/ojcollapsible',
'ojs/ojselectcombobox', 'ojs/ojselectsingle',
'ojs/ojdatetimepicker', 'ojs/ojswitch', 'ojs/ojslider',
 'ojs/ojlabel', 'ojs/ojtable',
'ojs/ojformlayout', 'ojs/ojlabelvalue', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojchart'],
 function(oj, ko, $, moment, Bootstrap, ResponsiveUtils, ResponsiveKnockoutUtils,
  ArrayDataProvider, ConverterUtilsI18n, DateTimeConverter, ThemeUtils) {

    function ServiceModel() {
      var self = this;
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
      };

 

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      self.getMock = function(){
        $.ajax({
          type: 'GET',
          url: `js/viewModels/zipcode.json`,
          
          success: function (data) {
              let selectOptions = [];
              let mapZipPrepare = {};
              data.forEach(element => {
                let option = {
                  "value":element.AREA_NUMBE,
                  "label" : element.COMMUNITY
                }
                mapZipPrepare[element.AREA_NUMBE] = {};
                mapZipPrepare[element.AREA_NUMBE]["the_geom"] = element.the_geom
              //  var centerpoint = element.centerpoint.substring(7,(element.centerpoint.length-1)).split(" ");
              var centerpoint = element.centerpoint
                mapZipPrepare[element.AREA_NUMBE]["centerpoint"] = {
                  // "lat":+centerpoint[0],
                  // "lng":+centerpoint[1]
                  "lat":+centerpoint.lat,
                  "lng":+centerpoint.lng
                }
                self.mapZip(mapZipPrepare);
                
                selectOptions.push(option);
              });
              self.zipCodes(selectOptions);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            data = JSON.parse(xhr.responseText); 

            
            let selectOptions = [];
              let mapZipPrepare = {};
              data.forEach(element => {
                let option = {
                  "value":element.ZIP,
                  "label" : element.ZIP
                }
                mapZipPrepare[element.ZIP] = element.the_geom
                self.mapZip(mapZipPrepare);
                selectOptions.push(option);
              });
              self.zipCodes(selectOptions);
              console.log(xhr.responseText)
          }
      });
      }
      self.getKpis = function(){
        $.ajax({
          type: 'GET',
          url: `js/viewModels/Final_cluster_data.json`,
          success: function (data) {

              let offenceTypes= [];
              let offenceObj = {};
              let offence =[];
              var pieSeries= [];
              var pinpoints = []
              
              var barSeries=[{name: "Series", items: []}];
              if(data && data.length){
                self.kpis(data);
                let areaSeries = [{name : "Series", items : [0,0,0,0,0,0,0]}];
                data.forEach(kpi=>{
                  if(offence.indexOf(kpi['Primary Type']) == -1){
                    offence.push({
                      "value":kpi['Primary Type'],
                      "label":kpi['Primary Type']
                    });
                  }
                  pinpoints.push({
                    "position": new google.maps.LatLng(kpi["Latitude"],kpi["Longitude"]),
                    "type":"info"
                  })
                  if(kpi.day_of_week){
                    switch (kpi.day_of_week.toUpperCase()){
                      case "SUNDAY":  areaSeries[0].items[0] += 1;
                                      break;
                      case "MONDAY":  areaSeries[0].items[1] += 1;
                                      break;
                      case "TUESDAY":  areaSeries[0].items[2] += 1;
                                      break;
                      case "WEDNESDAY":  areaSeries[0].items[3] += 1;
                                      break;
                      case "THURSDAY":  areaSeries[0].items[4] += 1;
                                      break;
                      case "FRIDAY":  areaSeries[0].items[5] += 1;
                                      break;
                      case "SATURDAY":  areaSeries[0].items[6] += 1;
                                      break;
                    }
                  }


                  if(kpi['Primary Type']){
                    if(offenceObj.hasOwnProperty(kpi['Primary Type'])){
                      offenceObj[kpi['Primary Type']] += 1
                    } else{
                      offenceObj[kpi['Primary Type']] = 1;
                    }
                  }
                });
                for(offenceName in offenceObj){
                  offenceTypes.push(offenceName); 
                  pieSeries.push({"name":offenceName, items:[offenceObj[offenceName]] })
                  barSeries[0].items.push(offenceObj[offenceName]);
                }
                self.pinMarkers(pinpoints);
                self.barSeriesValue(barSeries);
                self.barGroupsValue(offenceTypes);
                self.pieSeriesValue(pieSeries);
                self.areaSeriesValue(areaSeries);
                self.offenceType(offence);
                
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
      });
      }
      self.getCrimeRate = function(){
        $.ajax({
          type: 'GET',
          url: `js/viewModels/Crime_rate.json`,
          success: function (data) {
              if(data && data.length){
                self.crimeRates(data);
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
      });
      }
      self.getForeCast = function(){
        $.ajax({
          type: 'GET',
          url: `js/viewModels/forecast.json`,
          success: function (data) {
              if(data && data.length){
                self.foreCast(data);
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
      });
      }
    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return ServiceModel;
  }
);
