/**
 * @license
 * Copyright (c) 2014, 2020, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * @ignore
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'moment', 'accUtils', 'ojs/ojbootstrap',
  'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
  'ojs/ojarraydataprovider', 'ojs/ojconverterutils-i18n',
  'ojs/ojconverter-datetime', 'ojs/ojthemeutils',
  'ojs/ojknockout', 'ojs/ojbutton',
  'ojs/ojinputtext', 'ojs/ojinputnumber',
  'ojs/ojradioset', 'ojs/ojcheckboxset', 'ojs/ojcollapsible',
  'ojs/ojselectcombobox', 'ojs/ojselectsingle',
  'ojs/ojdatetimepicker', 'ojs/ojswitch', 'ojs/ojslider',
  'ojs/ojlabel', 'ojs/ojtable', 'ojs/ojlistview',
  'ojs/ojformlayout', 'ojs/ojlabelvalue', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojchart'],
  function (oj, ko, $, moment, accUtils, Bootstrap, ResponsiveUtils, ResponsiveKnockoutUtils,
    ArrayDataProvider, ConverterUtilsI18n, DateTimeConverter, ThemeUtils) {

    function DashboardViewModel() {
      var self = this;
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function () {
        //   accUtils.announce('Dashboard page loaded.', 'assertive');
        document.title = "Dashboard";
        var self = this;

        self.areaSeriesValue = ko.observableArray();
        self.barSeriesValue = ko.observableArray();
        self.pinMarkers = ko.observableArray([]);
        self.SelectLocationValue = ko.observable("AM");
        console.log(moment().format('ddd, ll'));
        self.regionSelected = ko.observable(false);
        self.elementStyle = ko.observable("oj-flex-item element")
        self.kpis = ko.observable();
        self.dateRange = ko.observable();
        self.dateRangeCal = ko.observable(null);
        self.displayModule = ko.observable('map');
        self.selectedTime = ko.observable();
        self.location = ko.observable();
        self.mapZip = ko.observable();
        self.selectedZips = ko.observable([]);
        self.barGroupsValue = ko.observableArray([]);
        self.pieSeriesValue = ko.observableArray([]);
        self.foreCast = ko.observableArray([])
        self.classsification = ko.observableArray([]);
        self.crimeRates = ko.observableArray([]);
        self.zipCodes = ko.observableArray([]);
        self.offenceType = ko.observableArray([]);
        self.selectedOffence = ko.observable();
        self.cluster = ko.observable("OFF")
        self.seriesValue= ko.observableArray();
        self.foreCastStore = [];
        self.classsificationStore = [];
        self.kpisList=[];
        self.crimeRateStore = [];
        self.description = ko.observable();
        self.descList = ko.observableArray([]);
        self.domain ="http://demo6785834.mockable.io/";
        self.descriptionList =new ArrayDataProvider(self.descList, { keyAttributes: 'Date' });
        self.foreCastList = new ArrayDataProvider(self.foreCast, { keyAttributes: 'Date' });
        self.classsificationList = new ArrayDataProvider(self.classsification, { keyAttributes: 'Date' });
        self.zipCodeList = new ArrayDataProvider(self.zipCodes, { keyAttributes: 'value' });
        self.crimeRatesList = new ArrayDataProvider(self.crimeRates, { keyAttributes: 'Date' });
        
        self.angles = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24];
        self.boatSpeeds5 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


        // Implement further logic if neede
        self.drinkValues = [
          { id: 'map', label: 'Map', icon: 'oj-ux-ico-align-left' },
          { id: 'kpi', label: 'Kpis', icon: 'oj-ux-ico-align-center' },
          { id: 'analysis', label: 'Analysis', icon: 'oj-ux-ico-align-right' },
          { id: 'classsification', label: 'Classsification', icon: 'oj-ux-ico-align-right' }
        ];
        self.timeValues = [
          { id: '6AM-2PM', label: '6AM-2PM' },
          { id: '2PM-10PM', label: '2PM-10PM' },
          { id: '10PM-6AM', label: '10PM-6AM' }
        ];
        var browsers = [
          // { value: 'all', label: 'All' },
          { value: 'week', label: 'Week' },
          { value: 'lastweek', label: 'Last Week' },
          { value: 'lastMonth', label: 'Last Month' },
          { value: 'last6Months', label: 'Last 6 Months' },
          { value: 'lastYear', label: 'Last Year' },
        ];

      var desc =[{value: 'OTHER', 'label': 'OTHER'},
      {value: 'PARKING LOT/GARAGE(NON.RESID.)',
       'label': 'PARKING LOT/GARAGE(NON.RESID.)'},
      {value: 'APARTMENT', 'label': 'APARTMENT'},
      {value: 'SIDEWALK', 'label': 'SIDEWALK'},
      {value: 'STREET', 'label': 'STREET'},
      {value: 'RESTAURANT', 'label': 'RESTAURANT'},
      {value: 'GROCERY FOOD STORE', 'label': 'GROCERY FOOD STORE'},
      {value: 'RESIDENTIAL YARD (FRONT/BACK)',
       'label': 'RESIDENTIAL YARD (FRONT/BACK)'},
      {value: 'COMMERCIAL / BUSINESS OFFICE',
       'label': 'COMMERCIAL / BUSINESS OFFICE'},
      {value: 'RESIDENCE', 'label': 'RESIDENCE'},
      {value: 'RESIDENCE-GARAGE', 'label': 'RESIDENCE-GARAGE'},
      {value: 'SMALL RETAIL STORE', 'label': 'SMALL RETAIL STORE'},
      {value: 'ALLEY', 'label': 'ALLEY'},
      {value: 'PARK PROPERTY', 'label': 'PARK PROPERTY'},
      {value: 'SCHOOL, PUBLIC, BUILDING', 'label': 'SCHOOL, PUBLIC, BUILDING'},
      {value: 'CTA STATION', 'label': 'CTA STATION'},
      {value: 'GAS STATION', 'label': 'GAS STATION'},
      {value: 'DEPARTMENT STORE', 'label': 'DEPARTMENT STORE'},
      {value: 'DRUG STORE', 'label': 'DRUG STORE'},
      {value: 'BANK', 'label': 'BANK'},
      {value: 'DAY CARE CENTER', 'label': 'DAY CARE CENTER'},
      {value: 'CTA BUS STOP', 'label': 'CTA BUS STOP'},
      {value: 'RESIDENCE PORCH/HALLWAY', 'label': 'RESIDENCE PORCH/HALLWAY'},
      {value: 'ATHLETIC CLUB', 'label': 'ATHLETIC CLUB'},
      {value: 'VEHICLE NON-COMMERCIAL', 'label': 'VEHICLE NON-COMMERCIAL'},
      {value: 'NURSING HOME/RETIREMENT HOME',
       'label': 'NURSING HOME/RETIREMENT HOME'},
      {value: 'DRIVEWAY - RESIDENTIAL', 'label': 'DRIVEWAY - RESIDENTIAL'},
      {value: 'CHURCH/SYNAGOGUE/PLACE OF WORSHIP',
       'label': 'CHURCH/SYNAGOGUE/PLACE OF WORSHIP'},
      {value: 'VACANT LOT/LAND', 'label': 'VACANT LOT/LAND'},
      {value: 'BAR OR TAVERN', 'label': 'BAR OR TAVERN'},
      {value: 'CTA TRAIN', 'label': 'CTA TRAIN'},
      {value: 'VEHICLE-COMMERCIAL', 'label': 'VEHICLE-COMMERCIAL'},
      {value: 'HOSPITAL BUILDING/GROUNDS', 'label': 'HOSPITAL BUILDING/GROUNDS'},
      {value: 'VEHICLE - DELIVERY TRUCK', 'label': 'VEHICLE - DELIVERY TRUCK'},
      {value: 'CHA APARTMENT', 'label': 'CHA APARTMENT'},
      {value: 'CAR WASH', 'label': 'CAR WASH'},
      {value: 'COLLEGE/UNIVERSITY GROUNDS',
       'label': 'COLLEGE/UNIVERSITY GROUNDS'},
      {value: 'TAXICAB', 'label': 'TAXICAB'},
      {value: 'OTHER COMMERCIAL TRANSPORTATION',
       'label': 'OTHER COMMERCIAL TRANSPORTATION'},
      {value: 'SCHOOL, PRIVATE, BUILDING', 'label': 'SCHOOL, PRIVATE, BUILDING'},
      {value: 'TAVERN/LIQUOR STORE', 'label': 'TAVERN/LIQUOR STORE'},
      {value: 'LIBRARY', 'label': 'LIBRARY'},
      {value: 'HOTEL/MOTEL', 'label': 'HOTEL/MOTEL'},
      {value: 'CONVENIENCE STORE', 'label': 'CONVENIENCE STORE'},
      {value: 'POLICE FACILITY/VEH PARKING LOT',
       'label': 'POLICE FACILITY/VEH PARKING LOT'},
      {value: 'CTA PLATFORM', 'label': 'CTA PLATFORM'},
      {value: 'CTA GARAGE / OTHER PROPERTY',
       'label': 'CTA GARAGE / OTHER PROPERTY'},
      {value: 'GOVERNMENT BUILDING/PROPERTY',
       'label': 'GOVERNMENT BUILDING/PROPERTY'},
      {value: 'WAREHOUSE', 'label': 'WAREHOUSE'},
      {value: 'CHA PARKING LOT/GROUNDS', 'label': 'CHA PARKING LOT/GROUNDS'},
      {value: 'SCHOOL, PUBLIC, GROUNDS', 'label': 'SCHOOL, PUBLIC, GROUNDS'},
      {value: 'AIRPORT TERMINAL LOWER LEVEL - NON-SECURE AREA',
       'label': 'AIRPORT TERMINAL LOWER LEVEL - NON-SECURE AREA'},
      {value: 'AIRPORT TERMINAL UPPER LEVEL - SECURE AREA',
       'label': 'AIRPORT TERMINAL UPPER LEVEL - SECURE AREA'},
      {value: 'AIRPORT VENDING ESTABLISHMENT',
       'label': 'AIRPORT VENDING ESTABLISHMENT'},
      {value: 'CTA BUS', 'label': 'CTA BUS'},
      {value: 'BARBERSHOP', 'label': 'BARBERSHOP'},
      {value: 'CHA HALLWAY/STAIRWELL/ELEVATOR',
       'label': 'CHA HALLWAY/STAIRWELL/ELEVATOR'},
      {value: 'LAKEFRONT/WATERFRONT/RIVERBANK',
       'label': 'LAKEFRONT/WATERFRONT/RIVERBANK'},
      {value: 'AIRPORT BUILDING NON-TERMINAL - NON-SECURE AREA',
       'label': 'AIRPORT BUILDING NON-TERMINAL - NON-SECURE AREA'},
      {value: 'AIRCRAFT', 'label': 'AIRCRAFT'},
      {value: 'ANIMAL HOSPITAL', 'label': 'ANIMAL HOSPITAL'},
      {value: 'SPORTS ARENA/STADIUM', 'label': 'SPORTS ARENA/STADIUM'},
      {value: 'OTHER RAILROAD PROP / TRAIN DEPOT',
       'label': 'OTHER RAILROAD PROP / TRAIN DEPOT'},
      {value: 'AIRPORT PARKING LOT', 'label': 'AIRPORT PARKING LOT'},
      {value: 'AIRPORT TERMINAL UPPER LEVEL - NON-SECURE AREA',
       'label': 'AIRPORT TERMINAL UPPER LEVEL - NON-SECURE AREA'},
      {value: 'AIRPORT EXTERIOR - NON-SECURE AREA',
       'label': 'AIRPORT EXTERIOR - NON-SECURE AREA'},
      {value: 'SAVINGS AND LOAN', 'label': 'SAVINGS AND LOAN'},
      {value: 'BRIDGE', 'label': 'BRIDGE'},
      {value: 'CONSTRUCTION SITE', 'label': 'CONSTRUCTION SITE'},
      {value: 'FEDERAL BUILDING', 'label': 'FEDERAL BUILDING'},
      {value: 'ABANDONED BUILDING', 'label': 'ABANDONED BUILDING'},
      {value: 'CLEANING STORE', 'label': 'CLEANING STORE'},
      {value: 'CURRENCY EXCHANGE', 'label': 'CURRENCY EXCHANGE'},
      {value: 'BOAT/WATERCRAFT', 'label': 'BOAT/WATERCRAFT'},
      {value: 'COIN OPERATED MACHINE', 'label': 'COIN OPERATED MACHINE'},
      {value: 'PAWN SHOP', 'label': 'PAWN SHOP'},
      {value: 'VEHICLE - OTHER RIDE SERVICE',
       'label': 'VEHICLE - OTHER RIDE SERVICE'},
      {value: 'ATM (AUTOMATIC TELLER MACHINE)',
       'label': 'ATM (AUTOMATIC TELLER MACHINE)'},
      {value: 'OTHER (SPECIFY)', 'label': 'OTHER (SPECIFY)'},
      {value: 'FIRE STATION', 'label': 'FIRE STATION'},
      {value: 'AIRPORT EXTERIOR - SECURE AREA',
       'label': 'AIRPORT EXTERIOR - SECURE AREA'},
      {value: 'SCHOOL, PRIVATE, GROUNDS', 'label': 'SCHOOL, PRIVATE, GROUNDS'},
      {value: 'CREDIT UNION', 'label': 'CREDIT UNION'},
      {value: 'MEDICAL/DENTAL OFFICE', 'label': 'MEDICAL/DENTAL OFFICE'},
      {value: 'AIRPORT BUILDING NON-TERMINAL - SECURE AREA',
       'label': 'AIRPORT BUILDING NON-TERMINAL - SECURE AREA'},
      {value: 'JAIL / LOCK-UP FACILITY', 'label': 'JAIL / LOCK-UP FACILITY'},
      {value: 'AIRPORT TERMINAL LOWER LEVEL - SECURE AREA',
       'label': 'AIRPORT TERMINAL LOWER LEVEL - SECURE AREA'},
      {value: 'CTA TRACKS - RIGHT OF WAY', 'label': 'CTA TRACKS - RIGHT OF WAY'},
      {value: 'FACTORY/MANUFACTURING BUILDING',
       'label': 'FACTORY/MANUFACTURING BUILDING'},
      {value: 'POOL ROOM', 'label': 'POOL ROOM'},
      {value: 'RESIDENCE - PORCH / HALLWAY',
       'label': 'RESIDENCE - PORCH / HALLWAY'},
      {value: 'AIRPORT TRANSPORTATION SYSTEM (ATS)',
       'label': 'AIRPORT TRANSPORTATION SYSTEM (ATS)'},
      {value: 'FOREST PRESERVE', 'label': 'FOREST PRESERVE'},
      {value: 'APPLIANCE STORE', 'label': 'APPLIANCE STORE'},
      {value: 'AIRPORT TERMINAL MEZZANINE - NON-SECURE AREA',
       'label': 'AIRPORT TERMINAL MEZZANINE - NON-SECURE AREA'},
      {value: 'MOVIE HOUSE/THEATER', 'label': 'MOVIE HOUSE/THEATER'},
      {value: 'AIRPORT/AIRCRAFT', 'label': 'AIRPORT/AIRCRAFT'},
      {value: 'HIGHWAY/EXPRESSWAY', 'label': 'HIGHWAY/EXPRESSWAY'},
      {value: 'COLLEGE/UNIVERSITY RESIDENCE HALL',
       'label': 'COLLEGE/UNIVERSITY RESIDENCE HALL'},
      {value: 'BOWLING ALLEY', 'label': 'BOWLING ALLEY'},
      {value: 'RESIDENCE - GARAGE', 'label': 'RESIDENCE - GARAGE'},
      {value: 'CEMETARY', 'label': 'CEMETARY'},
      {value: 'NEWSSTAND', 'label': 'NEWSSTAND'},
      {value: 'HOTEL / MOTEL', 'label': 'HOTEL / MOTEL'},
      {value: 'SCHOOL - PUBLIC BUILDING', 'label': 'SCHOOL - PUBLIC BUILDING'},
      {value: 'AUTO', 'label': 'AUTO'},
      {value: 'SCHOOL - PRIVATE BUILDING', 'label': 'SCHOOL - PRIVATE BUILDING'},
      {value: 'PARKING LOT / GARAGE (NON RESIDENTIAL)',
       'label': 'PARKING LOT / GARAGE (NON RESIDENTIAL)'},
      {value: 'VEHICLE - OTHER RIDE SHARE SERVICE (E.G., UBER, LYFT)',
       'label': 'VEHICLE - OTHER RIDE SHARE SERVICE (E.G., UBER, LYFT)'},
      {value: 'DELIVERY TRUCK', 'label': 'DELIVERY TRUCK'},
      {value: 'PORCH', 'label': 'PORCH'},
      {value: 'GAS STATION DRIVE/PROP.', 'label': 'GAS STATION DRIVE/PROP.'},
      {value: 'HOUSE', 'label': 'HOUSE'},
      {value: 'YARD', 'label': 'YARD'},
      {value: 'PARKING LOT', 'label': 'PARKING LOT'},
      {value: 'HALLWAY', 'label': 'HALLWAY'},
      {value: 'HOSPITAL', 'label': 'HOSPITAL'},
      {value: 'TRUCK', 'label': 'TRUCK'},
      {value: 'DRIVEWAY', 'label': 'DRIVEWAY'},
      {value: 'CHA PARKING LOT', 'label': 'CHA PARKING LOT'},
      {value: 'TAVERN', 'label': 'TAVERN'},
      {value: 'GANGWAY', 'label': 'GANGWAY'},
      {value: 'LIQUOR STORE', 'label': 'LIQUOR STORE'},
      {value: 'OFFICE', 'label': 'OFFICE'},
      {value: 'STAIRWELL', 'label': 'STAIRWELL'},
      {value: 'AUTO / BOAT / RV DEALERSHIP',
       'label': 'AUTO / BOAT / RV DEALERSHIP'},
      {value: 'VACANT LOT / LAND', 'label': 'VACANT LOT / LAND'},
      {value: 'RESIDENCE - YARD (FRONT / BACK)',
       'label': 'RESIDENCE - YARD (FRONT / BACK)'},
      {value: 'LAKEFRONT / WATERFRONT / RIVERBANK',
       'label': 'LAKEFRONT / WATERFRONT / RIVERBANK'},
      {value: 'POLICE FACILITY / VEHICLE PARKING LOT',
       'label': 'POLICE FACILITY / VEHICLE PARKING LOT'},
      {value: 'HOSPITAL BUILDING / GROUNDS',
       'label': 'HOSPITAL BUILDING / GROUNDS'},
      {value: 'EXPRESSWAY EMBANKMENT', 'label': 'EXPRESSWAY EMBANKMENT'},
      {value: 'RETAIL STORE', 'label': 'RETAIL STORE'},
      {value: 'VACANT LOT', 'label': 'VACANT LOT'},
      {value: 'GARAGE', 'label': 'GARAGE'},
      {value: 'ELEVATOR', 'label': 'ELEVATOR'},
      {value: 'POOLROOM', 'label': 'POOLROOM'},
      {value: 'CLEANERS/LAUNDROMAT', 'label': 'CLEANERS/LAUNDROMAT'},
      {value: 'RAILROAD PROPERTY', 'label': 'RAILROAD PROPERTY'},
      {value: 'CTA "L" TRAIN', 'label': 'CTA "L" TRAIN'},
      {value: 'MOVIE HOUSE / THEATER', 'label': 'MOVIE HOUSE / THEATER'},
      {value: 'HOTEL', 'label': 'HOTEL'},
      {value: 'LAGOON', 'label': 'LAGOON'},
      {value: 'SPORTS ARENA / STADIUM', 'label': 'SPORTS ARENA / STADIUM'},
      {value: 'SCHOOL YARD', 'label': 'SCHOOL YARD'},
      {value: 'VESTIBULE', 'label': 'VESTIBULE'},
      {value: 'NURSING HOME', 'label': 'NURSING HOME'},
      {value: 'LAUNDRY ROOM', 'label': 'LAUNDRY ROOM'},
      {value: 'BARBER SHOP/BEAUTY SALON', 'label': 'BARBER SHOP/BEAUTY SALON'},
      {value: 'CHURCH / SYNAGOGUE / PLACE OF WORSHIP',
       'label': 'CHURCH / SYNAGOGUE / PLACE OF WORSHIP'},
      {value: 'BASEMENT', 'label': 'BASEMENT'},
      {value: 'ROOMING HOUSE', 'label': 'ROOMING HOUSE'},
      {value: 'RIVER BANK', 'label': 'RIVER BANK'},
      {value: 'CHA HALLWAY', 'label': 'CHA HALLWAY'},
      {value: 'CLUB', 'label': 'CLUB'},
      {value: 'CHURCH', 'label': 'CHURCH'},
      {value: 'CTA PROPERTY', 'label': 'CTA PROPERTY'},
      {value: 'FACTORY / MANUFACTURING BUILDING',
       'label': 'FACTORY / MANUFACTURING BUILDING'},
      {value: 'NURSING / RETIREMENT HOME', 'label': 'NURSING / RETIREMENT HOME'},
      {value: 'CTA "L" PLATFORM', 'label': 'CTA "L" PLATFORM'},
      {value: 'GOVERNMENT BUILDING / PROPERTY',
       'label': 'GOVERNMENT BUILDING / PROPERTY'},
      {value: 'SCHOOL - PUBLIC GROUNDS', 'label': 'SCHOOL - PUBLIC GROUNDS'},
      {value: 'TAVERN / LIQUOR STORE', 'label': 'TAVERN / LIQUOR STORE'},
      {value: 'CHA PARKING LOT / GROUNDS', 'label': 'CHA PARKING LOT / GROUNDS'},
      {value: 'FARM', 'label': 'FARM'},
      {value: 'VEHICLE-COMMERCIAL - ENTERTAINMENT/PARTY BUS',
       'label': 'VEHICLE-COMMERCIAL - ENTERTAINMENT/PARTY BUS'},
      {value: 'VEHICLE-COMMERCIAL - TROLLEY BUS',
       'label': 'VEHICLE-COMMERCIAL - TROLLEY BUS'},
      {value: 'KENNEL', 'label': 'KENNEL'},
      {value: 'HORSE STABLE', 'label': 'HORSE STABLE'},
      {value: 'CHA GROUNDS', 'label': 'CHA GROUNDS'},
      {value: 'GARAGE/AUTO REPAIR', 'label': 'GARAGE/AUTO REPAIR'},
      {value: 'GOVERNMENT BUILDING', 'label': 'GOVERNMENT BUILDING'},
      {value: 'YMCA', 'label': 'YMCA'},
      {value: 'VEHICLE - COMMERCIAL', 'label': 'VEHICLE - COMMERCIAL'},
      {value: 'WOODED AREA', 'label': 'WOODED AREA'},
      {value: 'SCHOOL - PRIVATE GROUNDS', 'label': 'SCHOOL - PRIVATE GROUNDS'},
      {value: 'VEHICLE - OTHER RIDE SHARE SERVICE (LYFT, UBER, ETC.)',
       'label': 'VEHICLE - OTHER RIDE SHARE SERVICE (LYFT, UBER, ETC.)'},
      {value: 'MEDICAL / DENTAL OFFICE', 'label': 'MEDICAL / DENTAL OFFICE'},
      {value: 'COLLEGE / UNIVERSITY - RESIDENCE HALL',
       'label': 'COLLEGE / UNIVERSITY - RESIDENCE HALL'},
      {value: 'OTHER RAILROAD PROPERTY / TRAIN DEPOT',
       'label': 'OTHER RAILROAD PROPERTY / TRAIN DEPOT'},
      {value: 'CHA PLAY LOT', 'label': 'CHA PLAY LOT'},
      {value: 'CTA PARKING LOT / GARAGE / OTHER PROPERTY',
       'label': 'CTA PARKING LOT / GARAGE / OTHER PROPERTY'},
      {value: 'COLLEGE / UNIVERSITY - GROUNDS',
       'label': 'COLLEGE / UNIVERSITY - GROUNDS'},
      {value: 'CTA SUBWAY STATION', 'label': 'CTA SUBWAY STATION'},
      {value: 'BOAT / WATERCRAFT', 'label': 'BOAT / WATERCRAFT'},
      {value: 'TRAILER', 'label': 'TRAILER'},
      {value: 'HIGHWAY / EXPRESSWAY', 'label': 'HIGHWAY / EXPRESSWAY'},
      {value: 'CHA HALLWAY / STAIRWELL / ELEVATOR',
       'label': 'CHA HALLWAY / STAIRWELL / ELEVATOR'},
      {value: 'MOTEL', 'label': 'MOTEL'},
      {value: 'VEHICLE - COMMERCIAL: TROLLEY BUS',
       'label': 'VEHICLE - COMMERCIAL: TROLLEY BUS'},
      {value: 'VEHICLE - COMMERCIAL: ENTERTAINMENT / PARTY BUS',
       'label': 'VEHICLE - COMMERCIAL: ENTERTAINMENT / PARTY BUS'}];
        self.descList(desc);


        var offence = [
          { value: "all", label: 'All' },
          {value: "THEFT", label: "THEFT"},
        {value: "BATTERY", label: "BATTERY"},
        {value: "CRIMINAL DAMAGE", label: "CRIMINAL DAMAGE"},
        {value: "ASSAULT", label: "ASSAULT"},
        {value: "BURGLARY", label: "BURGLARY"},
        {value: "OTHER OFFENSE", label: "OTHER OFFENSE"},
        {value: "DECEPTIVE PRACTICE", label: "DECEPTIVE PRACTICE"},
        {value: "MOTOR VEHICLE THEFT", label: "MOTOR VEHICLE THEFT"},
        {value: "CRIMINAL TRESPASS", label: "CRIMINAL TRESPASS"},
        {value: "OFFENSE INVOLVING CHILDREN", label: "OFFENSE INVOLVING CHILDREN"},
        {value: "NARCOTICS", label: "NARCOTICS"},
        {value: "ROBBERY", label: "ROBBERY"},
        {value: "WEAPONS VIOLATION", label: "WEAPONS VIOLATION"},
        {value: "HOMICIDE", label: "HOMICIDE"},
        {value: "INTERFERENCE WITH PUBLIC OFFICER", label: "INTERFERENCE WITH PUBLIC OFFICER"},
        {value: "SEX OFFENSE", label: "SEX OFFENSE"},
        {value: "GAMBLING", label: "GAMBLING"},
        {value: "INTIMIDATION", label: "INTIMIDATION"},
        {value: "CRIM SEXUAL ASSAULT", label: "CRIM SEXUAL ASSAULT"},
        {value: "PUBLIC PEACE VIOLATION", label: "PUBLIC PEACE VIOLATION"},
        {value: "ARSON", label: "ARSON"},
        {value: "CRIMINAL SEXUAL ASSAULT", label: "CRIMINAL SEXUAL ASSAULT"},
        {value: "CONCEALED CARRY LICENSE VIOLATION", label: "CONCEALED CARRY LICENSE VIOLATION"},
        {value: "LIQUOR LAW VIOLATION", label: "LIQUOR LAW VIOLATION"},
        {value: "KIDNAPPING", label: "KIDNAPPING"},
        {value: "PROSTITUTION", label: "PROSTITUTION"}];
       
        self.dateRangeList = new ArrayDataProvider(browsers, { keyAttributes: 'value' });
        self.offenceList = new ArrayDataProvider(offence, { keyAttributes: 'value' });

        /** Calls to get Data & prepare Kpis  starts*/
        self.getMock();
        self.getForeCast();
        //self.getClassification();
        //self.getCrimeRate();
        self.getKpis();
        self.pieChart();
        self.barChart();
        self.lineChart();
        self.polarChart();
        self.loadMap();
        /** Calls to get Data & prepare Kpis  ends*/

        self.selectZip = self.selectZip.bind(this);
        self.selectDateRange = self.selectDateRange.bind(this);
        self.offenceChange = self.offenceChange.bind(this);
        self.timeChange = self.timeChange.bind(this);
        self.getResults = self.getResults.bind(this);


      };


      /**
               * Event handlers starts
               */
      /** On selection of Region */
      self.selectZip = function (time) {
        let self = this;
        
        if (self.location()) {
          self.regionSelected(true)
          self.selectedTime(null);
          self.elementStyle("oj-flex-item selectable-element");
          self.dateRange('all');
          self.dateRangeCal(null);
          self.selectedOffence('all')
        } else {
          self.regionSelected(false)
          self.elementStyle("oj-flex-item element");
        }
        let polyGeoLocation = {}
        polyGeoLocation = self.mapZip()[self.location()];
        self.selectRegionInMap(polyGeoLocation);
        let locations = {};
        /*let crimeRatesStore = [];
        for(let i=0; i< self.crimeRateStore.length; i++){
          if(self.crimeRateStore[i].Community_Area == self.location()){
            crimeRatesStore.push(self.crimeRateStore[i])
          }
          if(i == self.crimeRateStore.length-1){
            self.crimeRates(crimeRatesStore);
          }
        }*/
        
        self.selectDateRange();
      }

      self.selectDateRange = function (dom) {
        let self = this;
        //self.crimeRates([])
        if(dom != true){
          self.selectedTime(null);
        }
        return new Promise((res, rej) => {
          let selectedDate = self.dateRange();
          
          if (selectedDate && selectedDate != 'all') {
            let dateStart = "";
            let dateEnd = "";
            let kpisList = [];
            let filterCrimeRate = [];
            let result = {
              kpisList: [],
              filterCrimeRate: []
            }
            let today = moment().format('YYYY-MM-DD');
            switch (selectedDate) {
              case 'week':
                dateStart = moment().subtract(0, 'days').startOf('day').format('X');
                dateEnd = moment().subtract(6, 'days').endOf('day').format('X');
                break;
              case 'lastweek':
                dateStart = moment().subtract(7, 'days').startOf('day').format('X');
                dateEnd = moment(today).endOf('day').format('X');
                break;
              case 'lastMonth':
                dateStart = moment().subtract(1, 'months').startOf('month').format('X');
                dateEnd = moment().subtract(1, 'months').endOf('month').format('X');
                break;
              case 'last6Months':
                dateStart = moment().subtract(6, 'months').startOf('month').format('X');
                dateEnd = moment().subtract(6, 'months').endOf('month').format('X');
                break;
              case 'lastYear':
                dateStart = moment().subtract(1, 'years').startOf('year').format('X');
                dateEnd = moment().subtract(1, 'years').endOf('year').format('X');
                break;
              case 'next1Month':
                dateStart = moment().add(1, 'months').startOf('month').format('X');
                dateEnd = moment().add(1, 'months').endOf('month').format('X');
                break;
              default:
                console.log("differnt");
            }
            self.boatSpeeds5 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            for(let kpi =0; kpi < self.kpisList.length; kpi++){
              let ts = moment(self.kpisList[kpi].Date).format('X');
              let Community_Area = self.kpisList[kpi].Community_Area;
              if(ts >= dateStart && ts<=dateEnd && Community_Area == self.location()){
                kpisList.push(self.kpisList[kpi]);
                self.boatSpeeds5[self.kpisList[kpi]["hour"]] = self.boatSpeeds5[self.kpisList[kpi]["hour"]] +1;
              }
              if(kpi == self.kpisList.length-1){
                self.kpis(kpisList.length ? kpisList : []);
                self.prepareKPIStructure();  
                //self.selectZip()
                let polyGeoLocation = self.mapZip()[self.location()];
                self.selectRegionInMap(polyGeoLocation);
                self.seriesValue([
                  { items: getSeriesItems(self.angles, self.boatSpeeds5)},
                ]);        
                result.kpisList = kpisList;
              }
            };
            /*for (let crime = 0; crime < self.crimeRateStore.length; crime++) {
              let ts = moment(self.crimeRateStore[crime].Date).format('X');
              
              if (ts >= dateStart && ts <= dateEnd && self.crimeRateStore[crime].Community_Area == self.location()) {
                console.log(self.crimeRateStore[crime]);
                filterCrimeRate.push(self.crimeRateStore[crime]);
              }
              if (crime == self.crimeRateStore.length - 1) {
                self.crimeRates(filterCrimeRate.length > 0 ? filterCrimeRate : [])
                result.filterCrimeRate = filterCrimeRate;
                res(result);
              }
            };*/


          } else {
            /*let filterCrimeRate = [];
            for (let crime = 0; crime < self.crimeRateStore.length; crime++) {
              
              
              if (self.crimeRateStore[crime].Community_Area == self.location()) {
                
                filterCrimeRate.push(self.crimeRateStore[crime]);
              }
              if (crime == self.crimeRateStore.length - 1) {
                self.crimeRates(filterCrimeRate.length > 0 ? filterCrimeRate : [])
              }
            };*/

            self.kpis(self.kpisList);
            self.prepareKPIStructure();
            //self.selectZip()
            let kpisList = []
            //self.selectZip()
            for(let kpi =0; kpi < self.kpisList.length; kpi++){
              let Community_Area = self.kpisList[kpi].Community_Area;
              if(Community_Area == self.location()){

                kpisList.push(self.kpisList[kpi]);
                self.boatSpeeds5[self.kpisList[kpi]["hour"]] = self.boatSpeeds5[self.kpisList[kpi]["hour"]] +1;
              }
              if(kpi == self.kpisList.length-1){
                self.seriesValue([
                  { items: getSeriesItems(self.angles, self.boatSpeeds5)},
                ]);
              }
            }      


            let polyGeoLocation = self.mapZip()[self.location()];
            self.selectRegionInMap(polyGeoLocation);
            res({
              filterCrimeRate: self.crimeRateStore,
              kpisList: self.kpisList
            });
          }
        })
      }

      self.offenceChange = function () {
        let self = this;
        let selectedOffence = self.selectedOffence();
        return new Promise((res, rej) => {

          if (selectedOffence && selectedOffence !='all') {
            self.boatSpeeds5 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            self.selectDateRange(true).then((result) => {
              FCData = result.filterCrimeRate;
              KPData = result.kpisList;
              let filterCrimeRate = [];
              let kpisList = [];
              for(let kpi =0; kpi < KPData.length; kpi++){
                if(KPData[kpi]['Primary_Type'] ==selectedOffence ){
                  kpisList.push(KPData[kpi]);
                  self.boatSpeeds5[self.kpisList[kpi]["hour"]] = self.boatSpeeds5[self.kpisList[kpi]["hour"]] +1;
                }
                if(kpi == KPData.length-1){
                  self.kpis(kpisList.length ? kpisList : []);
                  self.prepareKPIStructure();
                  //self.selectZip()
                  let polyGeoLocation = self.mapZip()[self.location()];
                  self.selectRegionInMap(polyGeoLocation);
                  result.kpisList = kpisList;
                  self.seriesValue([
                    { items: getSeriesItems(self.angles, self.boatSpeeds5)},
                  ]); 
                }
              };
              /*for (let crime = 0; crime < FCData.length; crime++) {
                if (FCData[crime]['Primary_Type'] == selectedOffence && self.crimeRateStore[crime].Community_Area == self.location()) {
                  console.log(FCData[crime]);
                  filterCrimeRate.push(FCData[crime]);
                }
                if (crime == FCData.length - 1) {
                  self.crimeRates(filterCrimeRate.length ? filterCrimeRate : [])
                  result.filterCrimeRate = filterCrimeRate.length ? filterCrimeRate : [];
                  res(result);
                }
              };*/
              //self.crimeRates(filterCrimeRate)
            })
          } else {
            self.selectDateRange(true).then((result) => {
              self.kpis(result.kpisList);
              self.prepareKPIStructure();
              // self.selectZip()
              let polyGeoLocation = self.mapZip()[self.location()];
        self.selectRegionInMap(polyGeoLocation);
              res(result);
            });
          }
        });
      }

      self.timeChange = function () {
        let self = this;
        let time = self.selectedTime();
        if (time) {

          self.offenceChange().then((result) => {
            //Day_Period
            self.boatSpeeds5 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            FCData = result.filterCrimeRate;
            KPData = result.kpisList;
            let filterCrimeRate = [];
            let kpisList = [];
            for(let kpi =0; kpi < KPData.length; kpi++){
              if(KPData[kpi]['Day_Period'].toUpperCase() == time.toUpperCase()) {
                kpisList.push(KPData[kpi]);
                self.boatSpeeds5[self.kpisList[kpi]["hour"]] = self.boatSpeeds5[self.kpisList[kpi]["hour"]] +1;
              }
              if(kpi == KPData.length-1){
                self.kpis(kpisList.length ? kpisList : []);
                self.prepareKPIStructure();
                //self.selectZip(true)
                let polyGeoLocation = self.mapZip()[self.location()];
                self.selectRegionInMap(polyGeoLocation);
                result.kpisList = kpisList;
                self.seriesValue([
                  { items: getSeriesItems(self.angles, self.boatSpeeds5)},
                ]); 
              }
            };
            /*for (let crime = 0; crime < FCData.length; crime++) {
              if (FCData[crime]['Day_Period'].toUpperCase() == time.toUpperCase() && self.crimeRateStore[crime].Community_Area == self.location()) {
                console.log(FCData[crime]);
                filterCrimeRate.push(FCData[crime]);
              }
              if (crime == FCData.length - 1) {
                self.crimeRates(filterCrimeRate.length ? filterCrimeRate : [])
                result.filterCrimeRate = filterCrimeRate.length ? filterCrimeRate : [];
                //res(result);
              }
            };*/
          });
        }
      }
      self.clusterChange = function(){
        self.selectZip();
      }

      /**
       * Event handlers ends
       */

      /** 
       * 
       * Maps starts here
       */
      self.selectRegionInMap = function (coordinates) {
        if (coordinates) {

          self.map = new google.maps.Map(
            document.getElementById('googleMap'),
            { center: coordinates.centerpoint, zoom: 13 });
          var bermudaTriangle = new google.maps.Polygon({
            paths: coordinates.the_geom,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
          });
          bermudaTriangle.setMap(self.map);
        }
        let clusterArray = {};

        self.pinMarkers().forEach(pinPoint => {
          let fillColor = "";
          let label={};
          if(self.cluster()){
            // fillColor="#deecb8"
            // var contentString = '<div id="content">'+
            // '<h3 id="firstHeading" class="firstHeading">'+pinPoint.Community_Area+'</h3>'+
            // '<div id="bodyContent">'+
            // '<p><b>'+pinPoint.Primary_Type+'</b>'+
            // '</div>'+
            // '</div>';
      
            // var infowindow = new google.maps.InfoWindow({
            //   content: contentString
            // });

            // label= {
            //   text: pinPoint.Cluster.toString(),
            //   color: "black",
            //   fontSize: "16px",
            //   fontWeight: "bold"
            // }
          
          } else{
            if (pinPoint.Cluster == 1) {
              fillColor = "yellow"
            } else if (pinPoint.Cluster == 2) {
              fillColor = "red"
            } else if (pinPoint.Cluster == 0) {
              fillColor = "green"
            } else {
              fillColor = "blue"
            }
            label={}
          }
          if(self.cluster() && pinPoint.Community_Area == self.location()){
            if(clusterArray.hasOwnProperty(pinPoint.Cluster)){
              if(clusterArray[pinPoint.Cluster]["Primary_TypeCount"].hasOwnProperty(pinPoint.Primary_Type)){
                clusterArray[pinPoint.Cluster]["Primary_TypeCount"][pinPoint.Primary_Type]++;
              } else{
                clusterArray[pinPoint.Cluster]["Primary_TypeCount"][pinPoint.Primary_Type]=1;
              }
            } else {
              clusterArray[pinPoint.Cluster] = pinPoint;
              clusterArray[pinPoint.Cluster].count=1;
              clusterArray[pinPoint.Cluster]["Primary_TypeCount"] = {}
              clusterArray[pinPoint.Cluster]["Primary_TypeCount"][pinPoint.Primary_Type]=1
            }
            //   if (pinPoint.Community_Area == self.location())
            //   var marker = new google.maps.Marker({
            //     position: pinPoint.position,
            //     icon: {
            //       path: 'm 12,2.4000002 c -2.7802903,0 -5.9650002,1.5099999 -5.9650002,5.8299998 0,1.74375 1.1549213,3.264465 2.3551945,4.025812 1.2002732,0.761348 2.4458987,0.763328 2.6273057,2.474813 L 12,24 12.9825,14.68 c 0.179732,-1.704939 1.425357,-1.665423 2.626049,-2.424188 C 16.809241,11.497047 17.965,9.94 17.965,8.23 17.965,3.9100001 14.78029,2.4000002 12,2.4000002 Z',
            //       fillColor: fillColor,
            //       fillOpacity: 1.0,
            //       title:"sd",
            //       strokeColor: '#000000',
            //       strokeWeight: 1,
            //       scale: 2,
            //       anchor: new google.maps.Point(12, 24),
            //       labelOrigin: new google.maps.Point(12,8)
            //     },
            //     label: label,
            //     map: self.map
            //   });
            // }
           
            //   marker.addListener('mouseover', function() {
            //     infowindow.open(self.map, marker);
            //   });
            //   marker.addListener('mouseout', function(){
            //     infowindow.close(self.map, marker);
            //   })
            } else{
              if (pinPoint.Community_Area == self.location())
              var marker = new google.maps.Marker({
                position: pinPoint.position,
                icon: {
                  path: google.maps.SymbolPath.CIRCLE,
                  strokeWeight: 1,
                  strokeOpacity: 0.8,
                  fillColor: fillColor,
                  fillOpacity: 1.0,
                  title:"sd",
                  strokeColor: fillColor,
                  scale: 3
                },
                label: label,
                map: self.map
              });
            }
        })


        if(self.cluster()){
          
          let totalCluster={}
          
          let elementCluster="";
          let Community_Area = "";
          for(data in clusterArray){
            Community_Area = clusterArray[data].Community_Area;
            totalCluster[clusterArray[data].Primary_Type] = clusterArray[data].count;
            if(totalCluster[clusterArray[data].Primary_Type].count ){
              totalCluster[clusterArray[data].Primary_Type].count  = totalCluster[clusterArray[data].Primary_Type].count+ clusterArray[data]
            } else{
              totalCluster[clusterArray[data].Primary_Type].count  = 0;
            }
          }
          
          let clusterCounts={
            "0":0,
            "1":0,
            "2":0,
          }
          let completeOffence= 0;
            
          for(data in clusterArray){
            
            let pinPoint = clusterArray[data];
            for(tcdata in pinPoint.Primary_TypeCount){
              completeOffence = pinPoint.Primary_TypeCount[tcdata] +completeOffence;
            }
            clusterCounts[clusterArray[data].Cluster.toString()] = completeOffence
          }
          if(clusterCounts["0"] < clusterCounts["1"] && clusterCounts["0"] < clusterCounts["2"]){
            clusterCounts["0"] = 5;
            if(clusterCounts["1"] < clusterCounts["2"]){
              clusterCounts["1"] = 10;
              clusterCounts["2"] = 15; 
            } else{
              clusterCounts["2"] = 10;
              clusterCounts["1"] = 15;
            }
          } else if(clusterCounts["1"] < clusterCounts["0"] && clusterCounts["1"] < clusterCounts["2"]){
            clusterCounts["1"] = 5;
            if(clusterCounts["0"] < clusterCounts["2"]){
              clusterCounts["0"] = 10;
              clusterCounts["2"] = 15;
            } else{
              clusterCounts["2"] = 10;
              clusterCounts["0"] = 15;
            }
          } else{
            clusterCounts["2"] = 5;
            if(clusterCounts["0"] < clusterCounts["1"]){
              clusterCounts["0"] = 10;
              clusterCounts["1"] = 15;
            } else{
              clusterCounts["0"] = 15;
              clusterCounts["1"] = 10;
            }
          }
          completeOffence=0;
          for(data in clusterArray){
            let pinPoint = clusterArray[data];
            
            if(clusterArray[data].Cluster == 0){
              fillColor = "green"
            } else if(clusterArray[data].Cluster == 1){
              fillColor = "yellow"
            } else if(clusterArray[data].Cluster == 2){
              fillColor = "red"
            } else{
              fillColor = "blue"
            }
            

            label= {
              text: pinPoint.Cluster.toString(),
              color: "black",
              fontSize: "16px",
              fontWeight: "bold"
            }

            if (pinPoint.Community_Area == self.location()){
              let marker = new google.maps.Marker({
                position: pinPoint.position,
                icon: {
                  path: google.maps.SymbolPath.CIRCLE,
                  title:"sd",
                  strokeColor: fillColor,
                  strokeOpacity: 0.8,
                  fillColor: fillColor,
                  fillOpacity: 1.0,
                  scale: clusterCounts[clusterArray[data].Cluster.toString()]
                },
         //       label: label,
                map: self.map
              });
              
              let contentString="";
              completeOffence=0;
              elementCluster="";
              for(tcdata in pinPoint.Primary_TypeCount){
                completeOffence = pinPoint.Primary_TypeCount[tcdata] +completeOffence;
                elementCluster=elementCluster +"<div>"+pinPoint.Primary_TypeCount[tcdata]+" <b><small>"+tcdata+"</small></b></div>";
              }
  
              contentString = '<div id="content">'+
              '<h3 id="firstHeading" class="firstHeading">TotalCount:'+completeOffence+'</h3>'+
              '<div id="bodyContent">'+
              '<p>'+elementCluster+
              '</div>'+
              '</div>';

              let infowindow = new google.maps.InfoWindow({
                content: contentString
              });

              marker.addListener('mouseover', function() {
                infowindow.open(self.map, marker);
              });
              marker.addListener('mouseout', function(){
                infowindow.close(self.map, marker);
              })
            }
          }
        }
        
      }

      self.loadMap = function () {
        let self = this;

        self.map = new google.maps.Map(
          document.getElementById('googleMap'),
          { center: { "lat": 41.785, "lng": -87.751 }, zoom: 15 });
      }

      /**
       * 
       * Maps ENds here 
       */
      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function () {
        // Implement if needed
      };


      /***
       * 
       * KPIS  start from  here
       *  */
      self.pieChart = function () {
        self.threeDValue = ko.observable('off');
      }

      self.barChart = function () {
        /* toggle button variables */
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');

      }

      self.lineChart = function () {
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
        var areaGroups = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug","Sept", "Oct","Nov","Dec"];
        this.areaGroupsValue = ko.observableArray(areaGroups);
      }

        var getSeriesItems = function (angles, boatSpeeds) {
          var items = [], i;
          for (i = 0; i < angles.length; i++) {
            items.push({ x: angles[i], y: boatSpeeds[i] });
          }
          for (i = angles.length - 1; i >= 0; i--) {
            if (angles[i] != 180)
              items.push({ x: 360 - angles[i], y: boatSpeeds[i] });
          }
          return items;
        };

      self.polarChart = function () {

        // self.seriesValue = [
        //   { items: getSeriesItems(self.angles, self.boatSpeeds5) },
        // ];

        this.degreeConverter = {
          format: function (value) {
            return value + '\u00b0';
          }
        }

        this.speedConverter = {
          format: function (value) {
            return value + ' mph';
          }
        }
      }


      self.prepareKPIStructure = function () {
        let offenceTypes = [];
        let offenceObj = {};
        let dateRange={};
        let dateList=[];
        let dateTypes=[];
        let offence = [];
        var pieSeries = [];
        var pinpoints = []
        let data =self.kpis();
        var barSeries = [{ name: "Days", items: [] }];
        let areaSeries = [{ name: "Offence", items: [0, 0, 0, 0, 0, 0, 0,0,0,0,0,0] }];
        if (data && data.length) {
          
          
          data.forEach(kpi => {
            if(kpi && kpi.Community_Area == self.location()){
              if (offence.indexOf(kpi['Primary_Type']) == -1) {
                offence.push({
                  "value": kpi['Primary_Type'],
                  "label": kpi['Primary_Type']
                });
              }
              kpi.position =  new google.maps.LatLng(kpi["Latitude"], kpi["Longitude"]);
              pinpoints.push(kpi)
              if (kpi.Primary_Type) {
                //var areaGroups = ["SEX OFFENSE", "OFFENSE INVOLVING CHILDREN", "BATTERY", "THEFT", "OFFENSE INVOLVING CHILDREN", "DECEPTIVE PRACTICE", "CRIM SEXUAL ASSAULT"];
                switch (kpi.Month) {
                  case 1: areaSeries[0].items[0] += 1;
                    break;
                  case 2: areaSeries[0].items[1] += 1;
                    break;
                  case 3: areaSeries[0].items[2] += 1;
                    break;
                  case 4: areaSeries[0].items[3] += 1;
                    break;
                  case 5: areaSeries[0].items[4] += 1;
                    break;
                  case 6: areaSeries[0].items[5] += 1;
                    break;
                  case 7:
                    areaSeries[0].items[6] += 1;
                    break;
                  case 8:
                    areaSeries[0].items[7] += 1;
                    break;
                  case 9:
                    areaSeries[0].items[8] += 1;
                    break;
                  case 10:
                    areaSeries[0].items[9] += 1;
                    break;
                  case 11:
                    areaSeries[0].items[10] += 1;
                    break;
                  case 12:
                    areaSeries[0].items[11] += 1;
                    break;
                }
              }
              
              self.boatSpeeds5[kpi["hour"]] = self.boatSpeeds5[kpi["hour"]] +1;
              
              if (kpi['Primary_Type']) {
                if (offenceObj.hasOwnProperty(kpi['Primary_Type'])) {
                  offenceObj[kpi['Primary_Type']] += 1
                } else {
                  offenceObj[kpi['Primary_Type']] = 1;
                }
              }
  
              if (kpi['day_of_week']) {
                if (dateRange.hasOwnProperty(kpi['day_of_week'].toUpperCase())) {
                  dateRange[kpi['day_of_week'].toUpperCase()] += 1
                } else {
                  dateRange[kpi['day_of_week'].toUpperCase()] = 1;
                }
              }
            }
            
          });
          self.seriesValue([
            { items: getSeriesItems(self.angles, self.boatSpeeds5)},
          ]); 

          let days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
          days.forEach(datename => {

            dateTypes.push(datename);
            let count = dateRange[datename] ? dateRange[datename] : 0
            barSeries[0].items.push(dateRange[datename]);

          });
          for(offenceName in offenceObj){
            offenceTypes.push(offenceName);
            pieSeries.push({ "name": offenceName, items: [offenceObj[offenceName]] })
          }
          self.pinMarkers(pinpoints);
          self.barSeriesValue(barSeries);
          self.barGroupsValue(dateTypes);
          self.pieSeriesValue(pieSeries);
          self.areaSeriesValue(areaSeries);
          self.offenceType(offence);

        } else{
          self.pinMarkers(pinpoints);
          self.barSeriesValue(barSeries);
          self.barGroupsValue(dateTypes);
          self.pieSeriesValue(pieSeries);
          self.areaSeriesValue(areaSeries);
          self.offenceType(offence);

        }
      }

      /***
       * 
       * KPIS  ends
       *  */

      self.transitionCompleted = function () {
        // Implement if needed
      };

      /***
       * 
       * Services start from  here
       *  */
      self.getMock = function () {
        $.ajax({
          type: 'GET',
          url: `js/viewModels/zipcode.json`,

          success: function (data) {
            let selectOptions = [];
            let mapZipPrepare = {};
            data.forEach(element => {
              let option = {
                "value": element.COMMUNITY,
                "label": element.COMMUNITY
              }
              mapZipPrepare[element.COMMUNITY] = {};
              mapZipPrepare[element.COMMUNITY]["the_geom"] = element.the_geom
              //  var centerpoint = element.centerpoint.substring(7,(element.centerpoint.length-1)).split(" ");
              var centerpoint = element.centerpoint
              mapZipPrepare[element.COMMUNITY]["centerpoint"] = {
                // "lat":+centerpoint[0],
                // "lng":+centerpoint[1]
                "lat": +centerpoint.lat,
                "lng": +centerpoint.lng
              }
              self.mapZip(mapZipPrepare);

              selectOptions.push(option);
            });
            self.zipCodes(selectOptions);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            data = JSON.parse(xhr.responseText);


            let selectOptions = [];
            let mapZipPrepare = {};
            data.forEach(element => {
              let option = {
                "value": element.ZIP,
                "label": element.ZIP
              }
              mapZipPrepare[element.ZIP] = element.the_geom
              self.mapZip(mapZipPrepare);
              selectOptions.push(option);
            });
            self.zipCodes(selectOptions);
            console.log(xhr.responseText)
          }
        });
      }
      self.getKpis = function () {
        $.ajax({
          type: 'GET',
          url: `${self.domain}finalcluster`,
          success: function (data) {
            if (data && data.length) {
              self.kpis(data);
              self.kpisList = data;
              self.prepareKPIStructure(data)
            }

          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
        });
      }
      self.getCrimeRate = function () {
		  let self = this
		  let payload= 
          {"Date":self.dateRangeCal(),
          "Day_Period":self.selectedTime(),
          "Community_Area":self.location(),
          "Primary_Type":self.selectedOffence()};
		  //let payload = {"Date":"2019-03-01","Day_Period":"10pm-6am","Primary_Type":"OFFENSE INVOLVING CHILDREN","Community_Area":"JEFFERSON PARK"};
        $.ajax({
          type: 'POST',
          //url: `${self.domain}crimerate`,
		  url:"http://localhost:30/api/crime_rate_chicago",
		  data: JSON.stringify(payload),
          contentType: "application/json; charset=utf-8",
		  dataType: "json",
          success: function (data) {
            //if (data && data.length) {
            //  let cData = [];
            //  for (let i = 0; i < data.length; i++) {
             //   data[i].index = i;
            //    self.crimeRates.push(data[i])
             // }
              // self.crimeRates(data);
              // self.crimeRatesList = new ArrayDataProvider(data, { keyAttributes: 'Date' });
              //  self.crimeRatesList = new ArrayDataProvider(data, { keyAttributes: 'Date' });
			  data.index = 0;
			  //self.crimeRates.push(data);
			  self.crimeRates([data]);
              self.crimeRateStore = [data];

            //}

          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
        });
      }
      self.getForeCast = function () {
        $.ajax({
          type: 'GET',
          //url: self.domain+"forecast",
		  url:"http://localhost:30/api/forecast_api_chicago",
          success: function (data) {
            if (data && data.length) {
              self.foreCast(data);
              self.foreCastStore = data;
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
          }
        });
      }
      
      self.getClassification = function () {
        let self = this;
        console.log(self.location());
        console.log(self.dateRangeCal());
        console.log(self.selectedTime());
        let desc = self.descList()[self.description()];
        
        if(self.dateRangeCal() && self.selectedTime() && self.location() && desc && desc.value ){
          let payload= 
          {"Date":self.dateRangeCal(),
          "Day_Period":self.selectedTime(),
          "Community_Area":self.location(),
          "Location Description":desc.value}
          $.ajax({ 
            type: 'POST',
            //url: self.domain+"classsification",
			url: "http://localhost:30/api/crime_class_chicago",
            data: JSON.stringify(payload),
            contentType: "application/json; charset=utf-8",
		    dataType: "json",
            success: function (data) {
              if(data && data.Crime_Type_Probabilities){
                data.Crime_Type_Probabilities = JSON.stringify(data.Crime_Type_Probabilities);
                self.classsification([data]);
                self.classsificationStore = [data];
              }
            },
            error: function (xhr, ajaxOptions, thrownError) {
        
              console.log(xhr);
            }
          });
        }
     
        
      }
      
      self.getResults = function(){
        let self = this;
        self.getClassification();
		self.getCrimeRate();
      }
      /***
       * 
       * Services end  here
       *  */
      self.downloadXL = function () {
        var myTestXML = new accUtils(self.crimeRates());
        myTestXML.downLoad();

      }

    }

    /*
     * Returns an instance of the ViewModel providing one instance of the ViewModel. If needed,
     * return a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.
     */
    return DashboardViewModel;
  }
);
